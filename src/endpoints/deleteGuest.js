const guest = require("../data/guest.js");

module.exports = {
  method: 'delete',
  path: '/guest/:_id',
  async handler(request, response) {
    const MongoId = request.params._id;
    console.log("In Delete guest: MongoId = " + MongoId);
    const item = await guest.getOne(MongoId);
    console.log("In Delete guest: item after getOne = " + item);
    const isDeleteSuccessful = await guest.deleteOne(item);
    console.log("In Delete guest: isDeleteSuccessful after deleteOne = " + isDeleteSuccessful);
    if (isDeleteSuccessful) {
      response.status(204).json({status: 200,
        error: "Item not found",
        message: "Guest Deleted"});
    } else {
      response.status(404).json({
        status: 404,
        error: "guest not found",
        message: "ID does not exist"
      });
    }
  }
};
