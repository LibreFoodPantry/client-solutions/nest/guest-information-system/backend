const questionaire = require("../data/questionaire.js");

  module.exports = {
    method: 'get',
    path: '/qs/:barcode/:date1/:date2',
    async handler(request, response) {
      const barcode = request.params.barcode;
      const date1 = request.params.date1;
      const date2 = request.params.date2;
      const item = await questionaire.getRange(barcode, date1, date2);
      response.status(200).json(item);
      // if (item !== null) {
      //   response.status(200).json(item);
      // } else {
      //   response.status(404).json({
      //     status: 404,
      //     error: "QS not found",
      //     message: "ID does not exist"
      //   })
      // }
    }
};