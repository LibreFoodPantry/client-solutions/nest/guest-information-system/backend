const questionaire = require("../data/questionaire.js");

module.exports = {
  method: 'get',
  path: '/qs/:_id',
  async handler(request, response) {
    const MongoId = request.params._id;
    const item = await questionaire.getOne(MongoId);
    if (item !== null) {
      response.status(200).json(item);
    } else {
      response.status(404).json({
        status: 404,
        error: "QS not found",
        message: "ID does not exist"
      })
    }
  }
};
