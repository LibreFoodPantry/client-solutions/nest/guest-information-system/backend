const qs = require("../data/questionaire.js");

module.exports = {
  method: 'delete',
  path: '/qs/:_id',
  async handler(request, response) {
    const MongoId = request.params._id;
    console.log("In Delete qs: MongoId = " + MongoId);

    const item = await qs.getOne(MongoId);
    console.log("In Delete qs: item after getOne = " + item);
    const isDeleteSuccessful = await qs.deleteOne(item);
    console.log("In Delete qs: isDeleteSuccessful after deleteOne = " + isDeleteSuccessful);
    if (isDeleteSuccessful) {
      response.status(204).json({status: 200,
        error: "Item not found",
        message: "Questionaire Submission Deleted"});
    } else {
      response.status(404).json({
        status: 404,
        error: "Questionaire Submission not found",
        message: "ID does not exist"
      });
    }
  }
};
