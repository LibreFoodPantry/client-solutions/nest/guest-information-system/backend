const guest = require("../data/guest.js");

module.exports = {
  method: 'get',
  path: '/guest/:_id',
  async handler(request, response) {
    const MongoId = request.params._id;
    const item = await guest.getOne(MongoId);
    if (item !== null) {
      response.status(200).json(item);
    } else {
      response.status(404).json({
        status: 404,
        error: "Guest not found",
        message: "ID does not exist"
      })
    }
  }
};
