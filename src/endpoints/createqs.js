const qs = require("../data/questionaire.js");

module.exports = {
  method: 'post',
  path: '/qs',
  async handler(request, response) {
    const itemData = request.body;
    const item = await qs.create(itemData);
    const resourceUri = `${request.originalUrl}/${item.MongoId}`;
    response.status(201).location(resourceUri).json(item);
  }
};
