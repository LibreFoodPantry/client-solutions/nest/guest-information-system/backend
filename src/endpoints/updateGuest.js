const guest = require("../data/guest.js");

module.exports = {
  method: 'put',
  path: '/guest/:_id',
  async handler(request, response) {
    let id = request.params._id;
    let data = request.body;
    data._id = id;
    let item = await guest.update(data);
    if (item !== null) {
      response.status(200).json(item);
    } else {
      response.status(404).json({
        status: 404,
        error: "guest not found",
        message: "ID does not exist"
      })
    }
  }
};
