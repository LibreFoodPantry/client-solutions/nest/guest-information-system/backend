const guest = require("../data/guest.js");

module.exports = {
  method: 'get',
  path: '/guest',
  async handler(request, response) {
    const guest = await guest.getAll();
    response.status(200).json(guest);
  }
};
