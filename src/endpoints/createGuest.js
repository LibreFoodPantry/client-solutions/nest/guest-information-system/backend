
const guest = require("../data/guest.js");

module.exports = {
  method: 'post',
  path: '/guest',
  async handler(request, response) {
    const itemData = request.body;
    const item = await guest.create(itemData);
    const resourceUri = `${request.originalUrl}/${item.MongoId}`;
    response.status(201).location(resourceUri).json(item);
  }
};
