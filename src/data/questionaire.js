/**
 * questionaireionaire.js is responsible for manipulating the questionaireionaire collection in the
 * Mongo database. In architecture parlance, it is a Data Access Object.
 * It abstracts away the details of interact with the database.
 */
const Database = require("../lib/database.js");
const { ObjectID } = require("mongodb");


class questionaire {
  static async getAll() {
    const questionaireCollection = await getquestionaireCollection();
    const questionaire_cursor = questionaireCollection.find();
    let questionaire = await questionaire_cursor.toArray();
    questionaire.forEach(item => {
      item._id = item._id.toHexString();
    });
    return questionaire;
  }

  static async getOne(id) {
    const questionaireCollection = await getquestionaireCollection();
    let item = await questionaireCollection.findOne({ _id: ObjectID(id) });
    if (item !== null) {
      item._id = item._id.toHexString();
    }
    return item;
  }

  static async create(itemData) {
    const questionaireCollection = await getquestionaireCollection();
    const result = await questionaireCollection.insertOne(itemData);
    let item = await questionaireCollection.findOne({ _id: result.insertedId });
    item._id = item._id.toHexString();
    return item;
  }

  static async update(itemData) {
    const questionaireCollection = await getquestionaireCollection();
    const result = await questionaireCollection.updateOne(
      { _id: ObjectID(itemData._id) },
      { $set: { name: itemData.name } },
      { upsert: false }
    );
    if (result.modifiedCount < 1) {
      return null;
    } else {
      const item = await questionaireCollection.findOne(
        { _id: ObjectID(itemData._id) }
      );
      item._id = item._id.toHexString();
      return itemData;
    }
  }

  static async getRange(barcode, date1, date2) { 
    const questionaireCollection = await getquestionaireCollection();
    const questionaire_cursor = questionaireCollection.find();
    var B = new Date(date1);
    var C = new Date(date2);
    var D = new String(barcode);
    let range = [];
    let item = await questionaireCollection.find({guestid: barcode});
    let questionaire = await questionaire_cursor.toArray();
    questionaire.forEach(item => {
      var A = new Date(item.date);
      if (D == String(item.guestid) && A.getTime() >= B.getTime() && A.getTime() <= C.getTime()){
        range.push(item);
      }
    });
    while (D == String(item.guestid) && ( Date(item.date) >= Date(date1) && Date(item.date) <= Date(date2)) ){
    
    }
    return range;
  }
  static async deleteOne(itemData) {
    const questionaireCollection = await getquestionaireCollection();
    const result = await questionaireCollection.deleteOne(
      { _id: ObjectID(itemData._id) }
    );
    return result.deletedCount >= 1;
  }
}

async function getquestionaireCollection() {
  const database = await Database.get();
  return database.db("questionaire").collection("questionaire");
}


module.exports = questionaire;
