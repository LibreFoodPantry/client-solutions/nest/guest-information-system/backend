/**
 * guest.js is responsible for manipulating the guest collection in the
 * Mongo database. In architecture parlance, it is a Data Access Object.
 * It abstracts away the details of interact with the database.
 */
const Database = require("../lib/database.js");
const { ObjectID } = require("mongodb");


class guest {
  static async getAll() {
    const guestCollection = await getguestCollection();
    const guest_cursor = guestCollection.find();
    let guest = await guest_cursor.toArray();
    guest.forEach(item => {
      item._id = item._id.toHexString();
    });
    return guest;
  }

  static async getOne(id) {
    const guestCollection = await getguestCollection();
    let item = await guestCollection.findOne({ _id: ObjectID(id) });
    if (item !== null) {
      item._id = item._id.toHexString();
    }
    return item;
  }

  static async create(itemData) {
    const guestCollection = await getguestCollection();
    const result = await guestCollection.insertOne(itemData);
    let item = await guestCollection.findOne({ _id: result.insertedId });
    item._id = item._id.toHexString();
    return item;
  }

  static async update(itemData) {
    const guestCollection = await getguestCollection();
    const result = await guestCollection.updateOne(
      { _id: ObjectID(itemData._id) },
      { $set: { name: itemData.name } },
      { upsert: false }
    );
    if (result.modifiedCount < 1) {
      return null;
    } else {
      const item = await guestCollection.findOne(
        { _id: ObjectID(itemData._id) }
      );
      item._id = item._id.toHexString();
      return itemData;
    }
  }
  
  static async deleteOne(itemData) {
    const guestCollection = await getguestCollection();
    const result = await guestCollection.deleteOne(
      { _id: ObjectID(itemData._id) }
    );
    return result.deletedCount >= 1;
  }
}

async function getguestCollection() {
  const database = await Database.get();
  return database.db("guest").collection("guest");
}

module.exports = guest;
