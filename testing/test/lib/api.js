const axios = require('./axios.js');

module.exports = {
    async listGuest() {
        return axios.get("/guest");
    },

    async createGuest(data) {
        if (data === undefined) {
            data = { "name": "clown" };
        }
        if (data === null) {
            return axios.post("/guest");
        } else {
            return axios.post("/guest", data);
        }
    },

    async viewGuest(id) {
        return axios.get("/guest/" + id);
    },

    async updateGuest(item) {
        return axios.put("/guest/" + item._id, item);
    },

    async deleteGuest(id) {
        return axios.delete("/guest/" + id);
    },
};
