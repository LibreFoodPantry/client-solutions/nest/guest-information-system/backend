#!/usr/bin/env sh

cp -R /app/lib/nest-api.yaml /tmp/nest-api.yaml
echo "servers:" >> /tmp/nest-api.yaml
echo "  - url: '${SUT_BASE_URL}'" >> /tmp/nest-api.yaml

node ./node_modules/mocha/bin/mocha -C
